﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDropper : MonoBehaviour {
	private Vector3 StartPos;
	private float DropValue;
	[System.NonSerializedAttribute] public float DropTime = 0.5f;
	private float DropStartTime;
	private bool IsMinus;

	// Use this for initialization
	void Start () {
		StartPos = transform.position;
		DropStartTime = Time.timeSinceLevelLoad;
	}
	
	// Update is called once per frame
	void Update () {
		var diff = Time.timeSinceLevelLoad - DropStartTime;
		if (diff > DropTime) {
			transform.localPosition = StartPos + Vector3.down * DropValue;
			GameManager.Instance.OnCubeLanded(gameObject, IsMinus);
			if(IsMinus){
				Destroy(gameObject);
			}
			enabled = false;
		}

		var rate = diff / DropTime;
		transform.localPosition = Vector3.Lerp(StartPos, StartPos + Vector3.down * DropValue, rate);
	}

	public void SetDropValue(float dropValue, bool isMinus = false){
		DropValue = dropValue;
		IsMinus = isMinus;
	}
}
