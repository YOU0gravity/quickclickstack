﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour {
	[SerializeField] private GameObject CubePrefab;
	private float InsPosY = 0.1f;
	private float OneCubeThickness = 0.2f;
	private readonly float DropValue = 6f;

	public GameObject OneDrop(int insCount, Material mat, bool isMinus = false, int colorNumber = 3){			
		InsPosY = 0.1f + insCount * OneCubeThickness;
		Vector3 landingPos = new Vector3(0,InsPosY,0);
		GameObject cube = Instantiate(CubePrefab, landingPos + Vector3.up * DropValue, Quaternion.identity) as GameObject ;
		cube.transform.parent = transform;
		cube.name = colorNumber.ToString();
		float dropValue = DropValue;
		if(isMinus)
			dropValue += OneCubeThickness * 3;
		cube.GetComponent<CubeDropper>().SetDropValue(dropValue, isMinus);
		cube.GetComponent<MeshRenderer>().material = mat;
		return cube;
	}
}
