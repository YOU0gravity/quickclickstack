﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconEnlarger : MonoBehaviour {
	private Vector3 OriginScale;
	private float Rate = 1;
	private bool IsEnlarging;
	private float EnlargingSpeed = 0.2f;

	// Use this for initialization
	void Start () {
		OriginScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		if(IsEnlarging){
			Rate += Time.deltaTime * EnlargingSpeed;
			if(Rate > 1.1f)
				IsEnlarging = false;
		}else{
			Rate -= Time.deltaTime * EnlargingSpeed;
			if(Rate < 0.9f)
				IsEnlarging = true;
		}
		transform.localScale = OriginScale * Rate;
	}
}
