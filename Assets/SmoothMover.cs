﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothMover : MonoBehaviour {
	//Lerping
	private float StartTime;
	private Vector3 StartPos;
	private Vector3 EndPos;
	private bool IsLerping;
	private float LerpTime;
	private bool IsLocal;
	private bool HasCallBack;

	// Update is called once per frame
	void Update () {
		if(IsLerping){
			var diff = Time.timeSinceLevelLoad - StartTime;
			if (diff > LerpTime) {
				if(IsLocal){
					transform.localPosition = EndPos;
				}else{
					transform.position = EndPos;
				}
				
				if(HasCallBack)
					GameManager.Instance.OnLerpEnded();
				IsLerping = false;
			}
			var rate = diff / LerpTime;
			if(IsLocal){
				transform.localPosition = Vector3.Lerp(StartPos, EndPos, rate);
			}else{
				transform.position = Vector3.Lerp(StartPos, EndPos, rate);
			}
			
		}
	}
	public void StartLerping(float lerpTime = 1f){
		Vector3 endPos = transform.position;
		endPos.y = -1;
		StartLerping(lerpTime, endPos, false, true);
	}
	public void StartLerping(float lerpTime, Vector3 endPos, bool isLocal, bool hasCallBack){
		StartTime = Time.timeSinceLevelLoad;
		StartPos = transform.position;
		if(isLocal)
			StartPos = transform.localPosition;
		EndPos = endPos;
		LerpTime = lerpTime;
		IsLocal = isLocal;
		HasCallBack = hasCallBack;
		IsLerping = true;
	}
}
