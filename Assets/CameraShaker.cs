﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour {
	[SerializeField] private GameObject CameraObject;
	private float ShakeDecay = 0.004f;
	private Quaternion OriginLocalRot;
	private float ShakeIntensity;

	void Start(){
		OriginLocalRot = CameraObject.transform.localRotation;
	}
	// Update is called once per frame
	void Update () {
		if(this.ShakeIntensity > 0){
			Vector3 shakedVector = Random.insideUnitSphere * ShakeIntensity;
			shakedVector.z = 0;
			CameraObject.transform.localPosition = shakedVector;
			CameraObject.transform.localRotation = new Quaternion(OriginLocalRot.x + Random.Range(-ShakeIntensity, ShakeIntensity),
									OriginLocalRot.y + Random.Range(-ShakeIntensity, ShakeIntensity),
									OriginLocalRot.y + Random.Range(-ShakeIntensity, ShakeIntensity),
									OriginLocalRot.w + Random.Range(-ShakeIntensity, ShakeIntensity));
			
			this.ShakeIntensity -= this.ShakeDecay;

		}else if(this.ShakeIntensity < 0){
			this.ShakeIntensity = 0;
			this.CameraObject.transform.localPosition = Vector3.zero;
			this.CameraObject.transform.localRotation = this.OriginLocalRot;
		}
	}

	public void Shake(float intensity){
		this.ShakeIntensity = intensity;
	}
}