﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	private static GameManager _gameManager;
	public static GameManager Instance {
		get{ return _gameManager; }
	}

	[SerializeField] private CameraShaker _cameraShaker;
	[SerializeField] private CubeSpawner _cubeSpawner;
	[SerializeField] private ParticleSystem[] LandedParticles;
	[SerializeField] private GameObject CameraParent;
	private Vector3 OriginCameraPos;
	private Coroutine _changeColorCoroutine;

	//Game Start
	private bool IsStartButtonPressed;
	private bool IsGameStarted;
	[SerializeField] private GameObject TitleObject;
	[SerializeField] private Text CountDownText;


	//Time
	[SerializeField] private GameObject TimeObject;
	private float TimeLimit = 30;
	private int LastSec;
	[SerializeField] private GameObject[] Digit0;
	[SerializeField] private GameObject[] Digit1;
	[SerializeField] private GameObject[] Digit2;

	//Cube
	private int CubeInstantiatedCount;
	private int[] StackedCubeCount;
	[SerializeField] private Material[] CubeColorMaterial;
	private List<GameObject> CubeList;

	//Color
	private bool IsLightTurnedOn;
	private int CurrentColorNumber;//0: red, 1: blue, 2: white, 3: none
	[SerializeField] private Light SpotLight;
	private Color[] Colors;


	//Game End
	private bool IsGameEnded;
	[SerializeField] private GameObject SpotLightObject;
	[SerializeField] private GameObject NumbersObject;
	[SerializeField] private GameObject ScoreObject;
	[SerializeField] private GameObject IllustrativeText;
	[SerializeField] private GameObject RetryButton;

	//Sound
	private AudioSource _audioSource;
	[SerializeField] private AudioClip[] AudioClips;

	void Awake(){
		if(_gameManager == null){
			_gameManager = this;
		}else{
			Destroy(gameObject);
		}
		LastSec = (int)TimeLimit;

		Colors = new Color[4];
		Colors[0] = Color.red;
		Colors[1] = Color.blue;
		Colors[2] = Color.magenta;
		Colors[3] = Color.white;

		StackedCubeCount = new int[4];
		CubeList = new List<GameObject>();
	}

	// Use this for initialization
	void Start () {
		OriginCameraPos = CameraParent.transform.position;
		for(int i=0; i<10; i++){
			Digit0[i].transform.localPosition = new Vector3(-100,0,0);
			Digit1[i].transform.localPosition = new Vector3(-100,0,0);
			Digit2[i].transform.localPosition = new Vector3(-100,0,0);
		}
		ScoreObject.SetActive(false);
		RetryButton.SetActive(false);

		_audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(0, 45 * Time.deltaTime, 0);
		if(IsGameEnded)return;

		if(!IsGameStarted){
			if(!IsStartButtonPressed && Time.timeSinceLevelLoad > 0.5f){
				if(Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Mouse1)){
					IsStartButtonPressed = true;
					StartCoroutine(StartGameCoroutine());
				}
			}
			return;
		}

		if(Input.GetKeyDown(KeyCode.Mouse0) || 
				Input.GetKeyDown(KeyCode.LeftArrow) || 
				Input.GetKeyDown(KeyCode.A)){
			CreateAppropriateCube(0);
		}
		if(Input.GetKeyDown(KeyCode.Mouse1) || 
				Input.GetKeyDown(KeyCode.RightArrow) || 
				Input.GetKeyDown(KeyCode.D)){
			CreateAppropriateCube(1);
		}

		int leftTime = (int)(TimeLimit - Time.timeSinceLevelLoad);
		if(leftTime > 0){
			Debug.Log("LeftTime: " + leftTime);
			if(leftTime < LastSec){
				ChangeTime(leftTime);
			}
		}else{
			if(IsGameEnded == false){
				StopCoroutine(_changeColorCoroutine);
				SpotLight.enabled = true;
				CameraParent.GetComponent<SmoothMover>().StartLerping(1);
				ChangeTime(0);
				IsGameEnded = true;
			}
		}
	}
	public void OnLerpEnded(){
		StartCoroutine(OnLerpEndedCoroutine());
	}
	public IEnumerator OnLerpEndedCoroutine(){
		SpotLight.enabled = true;
		SpotLight.color = Color.white;
		SpotLightObject.GetComponent<SmoothMover>().StartLerping(0.5f, SpotLightObject.transform.localPosition + Vector3.down * 2.5f, true, false);
		NumbersObject.SetActive(false);
		TimeObject.SetActive(false);
		yield return new WaitForSeconds(0.5f);
		NumbersObject.transform.localPosition = NumbersObject.transform.localPosition + Vector3.down * 2.5f;
		NumbersObject.SetActive(true);

		
		if(CubeInstantiatedCount > 99)
			NumbersObject.transform.localPosition = NumbersObject.transform.localPosition + Vector3.right * 0.03f;

		ChangeTime(CubeInstantiatedCount);
		ScoreObject.SetActive(true);

		IllustrativeText.SetActive(false);
		RetryButton.SetActive(true);
	}

	private void CreateAppropriateCube(int inputNumber){
		if(IsGameEnded || !IsGameStarted) return;
		
		bool isCorrect = false;
		if(IsLightTurnedOn){
			switch(CurrentColorNumber){
				case 0: case 1:
					if(CurrentColorNumber == inputNumber) isCorrect = true;
					break;
				case 2: isCorrect = true; break;
				case 3: break;//Do nothing

			}
		}

		if(isCorrect){
			_cubeSpawner.OneDrop(CubeInstantiatedCount, CubeColorMaterial[CurrentColorNumber], false, CurrentColorNumber);
			CubeInstantiatedCount++;
		}else{
			_cubeSpawner.OneDrop(CubeInstantiatedCount, CubeColorMaterial[3], true, 3);
			if(CubeInstantiatedCount > 2){
				CubeInstantiatedCount -= 3;
			}else{
				CubeInstantiatedCount = 0;
			}
		}
	}
	IEnumerator ChangeColorCoroutine(){
		while(IsGameEnded == false){
			IsLightTurnedOn = !IsLightTurnedOn;
			SpotLight.enabled = IsLightTurnedOn;
			float waitTime = 0;
			if(IsLightTurnedOn){
				waitTime = Random.Range(0.8f, 3.3f);
				CurrentColorNumber = Random.Range(0, 3);
				SpotLight.color = Colors[CurrentColorNumber];
				yield return new WaitForSeconds(waitTime);
				for(int i=3; i>0; i--){
					_audioSource.PlayOneShot(AudioClips[2]);
					SpotLight.enabled = false;
					yield return new WaitForSeconds(i * 0.02f);
					SpotLight.enabled = true;
					yield return new WaitForSeconds(i * 0.05f);
				}
			}else{
				waitTime = Random.Range(0.5f, 1.3f);
				CurrentColorNumber = 3;//none
				yield return new WaitForSeconds(waitTime);
			}
		}
	}
	private void ChangeTime(int sec){
		int[] lastDigits = new int[3];
		int[] currentDigits = new int[3];

		lastDigits[0] = LastSec % 10;
		lastDigits[1] = (LastSec % 100) / 10;
		lastDigits[2] = LastSec / 100;
		currentDigits[0] = sec % 10;
		currentDigits[1] = (sec % 100) / 10;
		currentDigits[2] = sec / 100;

		for(int i=0; i<3; i++){
			if(lastDigits[i] != currentDigits[i]){
				switch(i){
					case 0:
							Digit0[lastDigits[i]].transform.localPosition = new Vector3(-100, 0, 0);
							Digit0[currentDigits[i]].transform.localPosition = new Vector3(0, 0, 0);
							break;
					case 1:
							Digit1[lastDigits[i]].transform.localPosition = new Vector3(-100, 0, 0);
							Digit1[currentDigits[i]].transform.localPosition = new Vector3(0, 0, 0);
							break;
					case 2:
							Digit2[lastDigits[i]].transform.localPosition = new Vector3(-100, 0, 0);
							Digit2[currentDigits[i]].transform.localPosition = new Vector3(0, 0, 0);
							break;
				}
			}			
		}
		LastSec = sec;
	}

	public void OnCubeLanded(GameObject cube, bool isMinus = false){
		ShakeCamera("Small");
		int cubeColorNumber = int.Parse(cube.name);
		LandedParticles[cubeColorNumber].transform.position = new Vector3(0, cube.transform.position.y, 0);
		LandedParticles[cubeColorNumber].Play();
		if(isMinus == false){
			_audioSource.PlayOneShot(AudioClips[0]);
			CubeList.Add(cube);
			if(IsGameEnded == false)
				CameraParent.transform.position = OriginCameraPos + Vector3.up * CubeInstantiatedCount * 0.2f;
		}else{
			_audioSource.PlayOneShot(AudioClips[1]);
			if(CubeList.Count > 0){
				for(int i=1; i<4; i++){
					if(CubeList.Count > i-1)
						Destroy(CubeList[CubeList.Count - i]);
				}
				for(int i=0; i<3; i++){
					if(CubeList.Count > i)
						CubeList.Remove(CubeList[CubeList.Count - 1]);
				}
			}
		}
	}

	private void ShakeCamera(string shakeLevel = ""){
		float intensity = 0.05f;
		switch(shakeLevel){
			case "Big":
				intensity = 0.05f;
				break;
			case "Middle":
				intensity = 0.03f;
				break;
			case "Small":
				intensity = 0.015f;
				break;
		}
		_cameraShaker.Shake(intensity);
	}

	private IEnumerator StartGameCoroutine(){
		float time = 0.8f;
		CountDownText.text = "3";
		yield return new WaitForSeconds(time);
		CountDownText.text = "2";
		yield return new WaitForSeconds(time);
		CountDownText.text = "1";
		yield return new WaitForSeconds(time);
		CountDownText.text = "START";
		yield return new WaitForSeconds(time);
		CountDownText.enabled = false;
		TimeLimit += Time.timeSinceLevelLoad;
		IsGameStarted = true;
		TitleObject.SetActive(false);
		_changeColorCoroutine =  StartCoroutine(ChangeColorCoroutine());
	}
	public void OnRetryButtonPressed(){
		UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
	}
	public void OnTweetButtonPressed(){
		string message = CubeInstantiatedCount.ToString() + "個積み上げた！";
		naichilab.UnityRoomTweet.Tweet ("quick-click-stack", message, "unity1week", "unityroom", "quickclickstack");
	}
}
